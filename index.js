// #3
console.log(fetch("https://jsonplaceholder.typicode.com/todos")
	.then(response => console.log(response.status)));
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json));


// #4
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => json.map(({title}) => title))
.then((titles) => console.log(titles));

// #5
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json));

// #7
fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: false,
		userId: 1,
		title: "Create to do the list"

	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// #8
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		id: 1,
		completed: false,
		userId: 1,
		title: "Updated Post"

	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// #9 - 10
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({

		title: "Update to do list",
		status: "Pending",
		dateCompleted: "Pending",
		userID: 1,
		description: "To update to do list with differenet structure"

	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// #10 - 11
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({


		status: "Completed",
		dateCompleted: "07/09/29",


	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// #12
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE"});





